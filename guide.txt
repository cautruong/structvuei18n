* PHP:
    * PHP > 7.1.3: https://windows.php.net/download#php-7.3
    * PHP Extensions: `curl, mbstring, openssl, pdo_pgsql, pgsql`
    * Redis server: https://redis.io/
    * Composer: https://getcomposer.org/
* Database:
    * Install PostgreSQL v9.6.6 and pgAdmin III.
    * In the dialog add new database: choose the Definition tab: Template=template0, Collation=C, Character type=C

* Git Client tool:
    * SmartGit: https://www.syntevo.com/smartgit/download/

* Create project laravel:
    * composer create-project --prefer-dist laravel/laravel project-name

* Add vue component into project laravel:
    * composer require laravel/ui
    * php artisan ui vue

* Run source local Unix OS:
    * In each source package have bin folder. Go to inside the source base directory
    * Open the terminal here.
    * Run command: php artisan serve
    * The website URL: http://localhost:8000

* Open file config/app.php edit url:
    * 'url' => env('APP_URL', 'http://localhost:8000'),
* Open file app/Providers/AppServiceProvider.php, add:
    * use Illuminate\Support\Facades\Schema;
    * Schema::defaultStringLength(191);

## III. Coding guide
* Localization
    * Link prefer: 
        * https://github.com/martinlindhe/laravel-vue-i18n-generator
    * In your project: 
        * composer require martinlindhe/laravel-vue-i18n-generator --dev
    * Register the service provider in config/app.php
        * MartinLindhe\VueInternationalizationGenerator\GeneratorProvider::class,
    * Next, publish the package default config:
        * php artisan vendor:publish --provider="MartinLindhe\VueInternationalizationGenerator\GeneratorProvider"
    * Using vue-i18n
        * npm i --save vue-i18n
    * Then generate the include file with
        * php artisan vue-i18n:generate
    * open resource/js/app.js, add:
        * import VueInternationalization from 'vue-i18n';
        * import Locale from './vue-i18n-locales.generated';
        * Vue.use(VueInternationalization);
        * const lang = document.documentElement.lang.substr(0, 2);
        // or however you determine your current app locale
        * const i18n = new VueInternationalization({
            locale: lang,
            messages: Locale
        });
        * const app = new Vue({
            el: '#app',
            i18n,
            data() {
                return {
                    isLoading: false,
                    isGlobalLoading: false,
                }
            },
            methods: {
                loading(val) {
                    this.isLoading = val;
                },
                fetchData(event) {
                    let componentRef = event.target.href.split('#')[1];
                    this.checkAndFetch(componentRef);
                },
                fetchDefault(defaultFetch = []) {
                    _.each(defaultFetch, v => {
                        this.checkAndFetch(v);
                    });
                },
                checkAndFetch(ref) {
                    if (_.isEmpty(this.$refs[ref])) {
                        return;
                    }
                    if (_.isFunction(this.$refs[ref].fetchData)) {
                        this.$refs[ref].fetchData();
                    }
                },
            }
        });
    * Open webpack.mix.js update:
        * mix.js('resources/js/app.js', 'public/js')
                .sass('resources/sass/app.scss', 'public/css');
                mix.webpackConfig({
                    plugins: [
                        new WebpackShellPlugin({
                            onBuildStart: [
                                'php artisan vue-i18n:generate'
                                //'php artisan config:js'
                            ],
                        }),
                    ],
                });
                
            mix.js('resources/js/app.js', 'public/js').version().sourceMaps()
                .sass('resources/sass/app.scss', 'public/css').version().sourceMaps();
            // Copy directory of images
            mix.copyDirectory([
                'resources/images'
            ], 'public/images');
    * create master layout: add body 
        <div id="app">
            <div v-if="isLoading || isGlobalLoading" class="d-flex h-100 w-100 justify-content-center preloader">
                <b-spinner label="Spinning" class="align-self-center"></b-spinner>
            </div>
            <!-- Content -->
            <main>
                @yield('contents')
                <footer></footer>
            </main>
        </div>
        <script src="js/app.js"></script>
        @yield('script')



    * Blade template: **{{ trans('file.key', ['name' => 'visitor']) }}** or **{{ __('file.key', ['name' => 'visitor']) }}**
    * Vue template: **{{ $t('file.key') }}** or **{{ $t('file.key', { name: 'visitor' }) }}**
    * Vue template js script: **this.$t('file.key')** or **this.$t('file.key', { name: 'visitor' })**





## II. Command line tools
* Database migration
    * Go to folder migration.
    * php artisan --env=[] migrate --seed                      Create schema + seed data
    * php artisan --env=[] migrate                             Create schema
    * php artisan --env=[] db:seed                             Seed data
    * php artisan --env=[] db:seed --class=UsersTableSeeder    Seed with specific class
    * php artisan --env=[] make:migration create_users_table   Create new migration file
    * php artisan --env=[] make:seeder UsersTable              Create new seed file
    * References: https://laravel.com/docs/5.7/migrations and https://laravel.com/docs/5.7/seeding
    
    
* Run source local Windows OS:
    * Install PHP and Composer: https://www.jeffgeerling.com/blog/2018/installing-php-7-and-composer-on-windows-10
    * Download PostgreSQL Database: https://www.enterprisedb.com/downloads/postgres-postgresql-downloads
    * Download PgAdmin 3: https://www.pgadmin.org/download/pgadmin-3-windows/
    * Create new PostgreSQL DB.
    * Install Redis server
        * Download file *Redis-x64-3.2.100.zip* from https://github.com/microsoftarchive/redis/releases/tag/win-3.2.100
        * Unzip and run file *redis-server.exe*
    * Install Git and Git Bash: https://git-scm.com/
    * Install Git Client tool (SmartGit or GitKraken).
    * Clone source code from gitlab server.
    * Open folder contains source code.
    * Run command: `composer install --no-dev`
    * Create file *.env* with content from file *.env.example*
    * Update content file *.env* with configuration of PostgreSQL DB, ORCA API.
    * Run command: `npm install --production`
    * Run command: `npm run production`
    * Run command: `php artisan key:generate`
    * Run command: `php artisan migrate`
    * Run command: `php artisan db:seed`
    * Run command: `php artisan storage:link`
    * Run command: `php artisan serve`
    * Open Web browser (Chrome, Firefox) and run url: http://localhost:8000

* Frontend
    * In root folder.
    * Run **npm run watch**
    * Run **php artisan config:js** Publish your Laravel configuration to Javascript
    * Run **php artisan vue-i18n:generate** Generates a vue-i18n compatible include file from your Laravel translations


