/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import VueInternationalization from 'vue-i18n';
import Locale from './vue-i18n-locales.generated';
import BootstrapVue from 'bootstrap-vue';
window.Vue = require('vue');
Vue.use(VueInternationalization);
Vue.use(BootstrapVue);

const lang = document.documentElement.lang.substr(0, 2);
// or however you determine your current app locale

const i18n = new VueInternationalization({
    locale: lang,
    messages: Locale
});
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */
Vue.component('example-component', require('./components/ExampleComponent.vue').default);
// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    i18n,
    data() {
        return {
            isLoading: false,
            isGlobalLoading: false,
        }
    },
    methods: {
        loading(val) {
            this.isLoading = val;
        },
        fetchData(event) {
            let componentRef = event.target.href.split('#')[1];
            this.checkAndFetch(componentRef);
        },
        fetchDefault(defaultFetch = []) {
            _.each(defaultFetch, v => {
                this.checkAndFetch(v);
            });
        },
        checkAndFetch(ref) {
            if (_.isEmpty(this.$refs[ref])) {
                return;
            }
            if (_.isFunction(this.$refs[ref].fetchData)) {
                this.$refs[ref].fetchData();
            }
        },
    }
});